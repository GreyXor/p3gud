module gitlab.com/GreyXor/p3gud

go 1.20

require (
	github.com/alexflint/go-arg v1.4.3
	github.com/briandowns/spinner v1.23.0
	github.com/cavaliergopher/grab/v3 v3.0.1
	github.com/dustin/go-humanize v1.0.1
)

require (
	github.com/alexflint/go-scalar v1.2.0 // indirect
	github.com/fatih/color v1.15.0 // indirect
	github.com/mattn/go-colorable v0.1.13 // indirect
	github.com/mattn/go-isatty v0.0.19 // indirect
	golang.org/x/sys v0.8.0 // indirect
	golang.org/x/term v0.8.0 // indirect
)
