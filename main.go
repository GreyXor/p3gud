package main

import (
	"crypto/tls"
	"encoding/xml"
	"io"
	"io/fs"
	"log"
	"net/http"
	"os"
	"path"
	"strconv"
	"strings"
	"time"

	"github.com/alexflint/go-arg"
	"github.com/briandowns/spinner"
	"github.com/cavaliergopher/grab/v3"
	"github.com/dustin/go-humanize"
)

const (
	programName    = "PS3GUD"
	programVersion = "1.0.0"
)

const RetailUpdatesEnv = "https://a0.ww.np.dl.playstation.net/tpl/np/TITLEID/TITLEID-ver.xml"

type TitlePatch struct {
	XMLName xml.Name `xml:"titlepatch"`
	Status  string   `xml:"status,attr"`
	TitleID string   `xml:"titleid,attr"`
	Tag     Tag      `xml:"tag"`
}

type Tag struct {
	XMLName      xml.Name  `xml:"tag"`
	Name         string    `xml:"name,attr"`
	Popup        string    `xml:"popup,attr"`
	SignOff      string    `xml:"signoff,attr"`
	MinSystemVer string    `xml:"min_system_ver,attr"`
	Packages     []Package `xml:"package"`
}

type Package struct {
	XMLName      xml.Name `xml:"package"`
	Version      string   `xml:"version,attr"`
	Size         string   `xml:"size,attr"`
	SHA1Sum      string   `xml:"sha1sum,attr"`
	URL          string   `xml:"url,attr"`
	PS3SystemVer string   `xml:"ps3_system_ver,attr"`
	ParamSFO     ParamSFO `xml:"paramsfo"`
}

type ParamSFO struct {
	XMLName xml.Name `xml:"paramsfo"`
	Title   string   `xml:"TITLE"`
}

// Args represents all the possible subcommands, arguments and flags that can be sent to the application.
type Args struct {
	TitleID string `help:"TitleID is the identification number of the game."` // TitleID represents the identification number of the game. https://www.psdevwiki.com/ps3/Productcode#Physical
}

// Description returns the software description (--help argument).
func (Args) Description() string {
	return programName + " allows you to automatically download all updates of an PS3 game directly from PSN servers."
}

// Version returns the software version (--version argument).
func (Args) Version() string {
	return programName + " " + programVersion
}

func main() {
	// Retrieve arguments.
	args := &Args{}
	arg.MustParse(args)

	// Disable TLS verifying because of the bad Sony certificates.
	http.DefaultTransport.(*http.Transport).TLSClientConfig = &tls.Config{InsecureSkipVerify: true}

	res, err := http.Get(strings.Replace(RetailUpdatesEnv, "TITLEID", args.TitleID, 2))
	if err != nil {
		log.Fatal(err)
	}

	body, err := io.ReadAll(res.Body)
	if err != nil {
		log.Fatal(err)
	}

	err = res.Body.Close()
	if err != nil {
		log.Fatal(err)
	}

	// we initialize our Users array
	var titlePatch TitlePatch
	// we unmarshal our byteArray which contains our
	// xmlFiles content into 'users' which we defined above
	err = xml.Unmarshal(body, &titlePatch)
	if err != nil {
		log.Fatal(err)
	}

	titleName := titlePatch.Tag.Packages[len(titlePatch.Tag.Packages)-1].ParamSFO.Title + " [" + titlePatch.TitleID + "]"

	err = os.MkdirAll(titleName, fs.FileMode(0o777))
	if err != nil {
		log.Fatal(err)
	}

	totalSize := uint64(0)
	for _, element := range titlePatch.Tag.Packages {
		atoi, err := strconv.Atoi(element.Size)
		if err != nil {
			return
		}
		totalSize += uint64(atoi)
	}

	log.Println(strconv.Itoa(len(titlePatch.Tag.Packages)+1) + " updates found for " + titleName + ". Size : " + humanize.Bytes(totalSize))

	s := spinner.New(spinner.CharSets[14], 50*time.Millisecond)
	defer s.Stop()
	s.Start()

	for index, element := range titlePatch.Tag.Packages {
		atoi, _ := strconv.Atoi(element.Size)
		s.Suffix = " " + element.Version + " - " + humanize.Bytes(uint64(atoi)) + " (" + strconv.Itoa(index+1) + "/" + strconv.Itoa(len(titlePatch.Tag.Packages)+1) + ")"
		_, err := grab.Get(path.Join(titleName, element.Version)+".pkg", element.URL)
		if err != nil {
			log.Fatal(err)
		}
	}
}
