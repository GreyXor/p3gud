# P3GUD

PlayStation 3 Games Updates Downloader

Based on : https://a0.ww.np.dl.playstation.net/tpl/np/{game_id}/{game_id}-ver.xml

![Demo](demo.jpg)

## Features

- Can resume the download where it left off
- ... 

## How to use ?

Download Gran Turismo 6 updates `go run main.go --titleid NPEA00502`.

## How to find titleid ?

- https://serialstation.com/
- https://rpcs3.net/compatibility
